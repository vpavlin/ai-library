Steps to deploy OpenWhisk actions
=================================
1. cd ansible
2. ansible-playbook roles/deploy_actions/deploy.yml --extra-vars "s3Endpoint=<> s3AccessKey=<> s3SecretKey=<> s3Bucket=<>"

