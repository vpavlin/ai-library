import datetime
import time
import csv
import io
import os
import boto3
import uuid
import argparse
import subprocess
import storage as ceph
import zipfile

parser=argparse.ArgumentParser()
parser.add_argument('-model', help='Model location', default='data/flake/05292018/tests-learn-5-3.model')
parser.add_argument('-s3Path', help='Source data location (Ceph bucket subfolder)', default='None')
parser.add_argument('-s3endpointUrl', help='Ceph backend endpoint URL', default='None')
parser.add_argument('-s3objectStoreLocation', help='Ceph Store (Bucket) Name', default='DH-DEV-DATA')
parser.add_argument('-s3accessKey', help='Ceph Store Access Key', default='None')
parser.add_argument('-s3secretKey', help='Ceph Store Secret key', default='None')
args=parser.parse_args()

# Create S3 session to access Ceph backend and get an S3 resource
s3 = ceph.create_session_and_resource(args.s3accessKey, args.s3secretKey, args.s3endpointUrl)

#Extract filename from destination url
modelurl = args.model.split("/")
modelfile = modelurl[-1]

# Download file from ceph backend in to the current working directory under 
# the same file name as the source

ceph.download_data(s3, args.s3objectStoreLocation, args.model, "flake-analysis/bots/images/" + modelfile)
listcmd = "ls -l flake-analysis/bots/images"
os.system(listcmd)


#Extract filename from destination url
urlsplit = args.s3Path.split("/")
filename = urlsplit[-1]
folder = '/'.join(urlsplit[:-1])

# Download file from ceph backend in to the current working directory under 
# the same file name as the source

ceph.download_data(s3, args.s3objectStoreLocation, args.s3Path, filename)

listcmd = "ls -l "
os.system(listcmd)

#Unzip the test failures data
z=zipfile.ZipFile(filename)
z.extractall()
filelist = z.namelist()
dirname = filelist.pop(0)
print(filelist)
listcmd = "ls -l " + filename
os.system(listcmd)

for item in filelist:
 cmd = "cat " + item + " | python flake-analysis/bots/tests-policy " + item + " | grep Flake > result.txt ; cat result.txt" 
 result = os.popen(cmd).read()
 print(item)
 print(result)
 f = open(item, 'a') 
 f.write(result)
 f.close()

#Zip results folder to uploade in to Ceph backend

zf = zipfile.ZipFile("results.zip", "w")
for dirname, subdirs, files in os.walk(dirname):
    zf.write(dirname)
    for filename in files:
        zf.write(os.path.join(dirname, filename))
zf.close()

print("Prediction complete..!")

destination = folder + "/results/results.zip"

# Upload file to the destination folder in the Ceph backend
ceph.upload_data(s3, args.s3objectStoreLocation, "results.zip", destination)


