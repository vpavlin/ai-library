spacy==2.0.11
pycorenlp==0.3.0
boto3==1.7.36
requests==2.18.4
textblob==0.15.1
vaderSentiment==3.2.1