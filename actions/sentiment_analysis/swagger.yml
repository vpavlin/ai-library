swagger: "2.0"
info:
  description: "Open Data Hub AI Library REST API"
  version: "1.0.0"
  title: "Open Data Hub"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
basePath: "/api/v1/namespaces/_/actions"
tags:
- name: "sentiment"
  description: "Sentiment and named entity relationship (NER) detection for text."
schemes:
- "https"
paths:
  /sentiment/ner-sentiment-svc:
    post:
      tags:
      - "sentiment"
      summary: "Detect the sentiment and NER of given text."
      description: ""
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Text to be analyzed."
        required: true
        schema:
          $ref: "#/definitions/NewSentiment"
      responses:
        200:
          description: "Sentiment response."
          schema:
            $ref: "#/definitions/Sentiment"
      security:
      - basic_auth: []
securityDefinitions:
  basic_auth:
    type: "basic"
definitions:
  NewSentiment:
    type: "object"
    properties:
      requestId:
        type: "string"
        description: "Optional ID of record. If present, the data associated with the request ID provided will be overridden.  If not provided, one will be generated."
        example: "f50ec0b7-f960-400d-91f0-c42a6d44e3d0"
      data:
        type: "object"
        description: "Text to be analyzed."
        example: {"text":"Really nicer than I expected for the price. I looked online for Izod sizing charts and it seems Izod has several charts that are inconsistent with each other so that was no help at all.", "user": "MrPerson", "reviewIndex": 5, "additionalText": "Neither was Amazon's generic size chart. I took a chance and ordered 3XL. I love Amazon."}
      sentimentFields:
        type: "string"
        description: "Comma separated list of parameters in the data to analyze."
        example: "text,additionalText"
      type:
        type: "string"
        description: "Optional type of request.  If not provided, will be marked as general."
        example: "trip-report"
      discardData:
        type: "boolean"
        description: "If true, data will not be saved."
        example: false
  Sentiment:
    type: "object"
    properties:
      requestId:
        type: "string"
        description: "Unique ID of request"
        example: "123e4567-e89b-12d3-a456-426655440000"
      processed:
        type: "number"
        description: "Number of records successfully processed."
        example: 1
      failed:
        type: "number"
        description: "Number of records failed processing."
        example: 0
      sentimentFields:
        type: "string"
        description: "Comma separated list of parameters in the data to analyze."
        example: "text,additionalText"
      type:
        type: "string"
        description: "Type of request."
        example: "trip-report"
      sentiment:
        type: "array"
        items:
          type: "object"
          properties:
            name:
              type: "string"
              description: "Name of the entity."
              example: "Amazon"
            category:
              type: "array"
              items:
                type: "string"
                description: "Category of entity."
                example: "ORG"
            count:
              type: "number"
              description: "Number of times the entity was analyzed."
              example: 1
            sentiment:
              type: "object"
              properties:
                verypositive:
                  type: "number"
                  description: "Number of times a very positive sentiment was detected for the entity."
                positive:
                  type: "number"
                  description: "Number of times a positive sentiment was detected for the entity."
                neutral:
                  type: "number"
                  description: "Number of times a neutral sentiment was detected for the entity."
                negative:
                  type: "number"
                  description: "Number of times a negative sentiment was detected for the entity."
                verynegative:
                  type: "number"
                  description: "Number of times a very negative sentiment was detected for the entity."
              example: {"positive": 1, "negative": 1}
externalDocs:
  description: "Find out more about Open Data Hub"
  url: "https://gitlab.com/opendatahub/"
