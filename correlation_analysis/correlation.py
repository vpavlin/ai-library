import os
import sys
import traceback
import s3fs
import statscomp.analysis_nc as nc
import statscomp.analysis_n as on
import statscomp.analysis_nn as nn
import statscomp.analysis_c as oc
import statscomp.analysis_cc as cc
import statscomp.analysis_multi as mf
import statscomp.utilities as uf
import json
import argparse
from datetime import datetime as dtm
import csv
import psycopg2
import pyarrow.parquet as pq

def callDirector(df, colNames, colTypes):
    """This function calls the statistical functions based on the type of columns.

    Args:
        df (pandas.DataFrame): The dataframe that columns to be analysed.
        colNames (list): The list of column names to be analysed.
        colTypes (list): The list of column types (numerical or categorical) for each column name in colName.

    Returns:
        list: A list of plotly graph dictionaries.
    """
    
    print("Running analysis...")
    if len(colTypes) == 1:
        if colTypes[0] == 'Numeric':
            return OneNumZeroCat(df, colNames, colTypes)
        elif colTypes[0] == 'Categorical':
            return ZeroNumOneCat(df, colNames, colTypes)
        else:
            raise ValueError('Incorrect value of colTypes')
    elif len(colTypes) == 2:
        if set(colTypes) == {'Numeric'}:
            return TwoNumZeroCat(df, colNames, colTypes)
        elif set(colTypes) == {'Numeric', 'Categorical'}:
            return OneNumOneCat(df, colNames, colTypes)
        elif set(colTypes) == {'Categorical'}:
            return ZeroNumTwoCat(df, colNames, colTypes)
    else:
        return multiFeatures(df, colNames, colTypes)


def OneNumOneCat(df, colNames, colTypes):
    """This function calls the statisitcal methods that generate output graphs.
    
    Args:
        df (pandas.DataFrame): The pandas dataframe that contains data columns to be analysed.
        colNames (list): The list of column names to be analysed.
        colTypes (list): The list of column types (numerical or categorical) for each column name in colName. In this case, ['Categorical','Numeric'].
    
    Returns:
        list: A list of plotly graph dictionaries.
    """
    
    df = uf.validate(df, colNames, colTypes)
    type_col, df, top_10 = nc.getTop_10(df, colNames, colTypes)
    g = [nc.cat_vs_num_recs(df, type_col)]
    df = uf.downsampleNumCat(top_10, type_col)
    # g = prob_dist(top_10,type_col) # gives error for model_name, cache_size but works for vendor, cache_size
    return (g + [nc.num_attr_spread(df, type_col), nc.box_plots(df, type_col)])   


def OneNumZeroCat(df, colNames, colTypes):
    """This function calls the statisitcal methods that generate output graphs.
    
    Args:
        df (pandas.DataFrame): The dataframe that columns to be analysed.
        colNames (list): The list of column names to be analysed.
        colTypes (list): The list of column types (numerical or categorical) for each column name in colName. In this case, ['Numeric'].
    
    Returns:
        list: A list of plotly graph dictionaries.
    """
    
    df = uf.validate(df,colNames, colTypes)
    g = [mf.numericalDescription(df, colNames), on.oneNumBar(df, colNames)]
    df = uf.downsampleNum(df, colNames)
    return (g + [on.oneNumBox(df, colNames) , on.oneNumDist(df, colNames)])


def TwoNumZeroCat(df, colNames, colTypes):
    """This function calls the statisitcal methods that generate output graphs.
    
    Args:
        df (pandas.DataFrame): The dataframe that columns to be analysed.
        colNames (list): The list of column names to be analysed.
        colTypes (list): The list of column types (numerical or categorical) for each column name in colName. In this case, ['Numeric','Numeric'].
    
    Returns:
        list: A list of plotly graph dictionaries.
    """
    
    df = uf.validate(df, colNames, colTypes)
    g = [mf.numericalDescription(df, colNames), nn.corr(df, colNames)]
    df = uf.downsampleNum(df, colNames)
    return (g + [nn.boxPlotComparison(df, colNames), nn.skewComparison(df, colNames), nn.skewConclusion(df, colNames), nn.scatter(df, colNames)])


def ZeroNumOneCat(df, colNames, colTypes):
    """This function calls the statisitcal methods that generate output graphs.
    
    Args:
        df (pandas.DataFrame): The dataframe that columns to be analysed.
        colNames (list): The list of column names to be analysed.
        colTypes (list): The list of column types (numerical or categorical) for each column name in colName. In this case, ['Categorical']
    
    Returns:
        list: A list of plotly graph dictionaries.
    """

    return([ mf.categoricalDescription(df, colNames), oc.charts(df, colNames)])
    

def ZeroNumTwoCat(df, colNames, colTypes):
    """This function calls the statisitcal methods that generate output graphs.

    Args:
        df (pandas.DataFrame): The dataframe that columns to be analysed.
        colNames (list): The list of column names to be analysed.
        colTypes (list): The list of column types (numerical or categorical) for each column name in colName. In this case, ['Categorical','Categorical'].

    Returns:
        list: A list of plotly graph dictionaries.
    """

    return([cc.countBarChart(df, colNames), cc.topAccounts(df, colNames), mf.categoricalDescription(df, colNames)])


def multiFeatures(df, colNames, colTypes):
    """This function calls the statisitcal methods that generate output graphs.

    Args:
        df (pandas.DataFrame): The dataframe that columns to be analysed.
        colNames (list): The list of column names to be analysed.
        colTypes (list): The list of column types (numerical or categorical) for each column name in colName.

    Returns:
        list: A list of plotly graph dictionaries.
    """
    
    colNumeric = []
    colCategorical = []
    for i in range(len(colTypes)):
        if colTypes[i] == "Numeric":
            colNumeric.append(colNames[i])
        else:
            colCategorical.append(colNames[i])
    graphs = []        
    if colNumeric:
        graphs.append(mf.numericalDescription(df, colNumeric))
        graphs.append(mf.correlationNumerical(df, colNumeric))
        
    if colCategorical:
        graphs.append(mf.correlationCategorical(df, colCategorical))
        graphs.append(mf.categoricalDescription(df, colCategorical))
    return (graphs)
    

    
if  __name__ == '__main__':
    
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-parser', help='Parser name', default='None')
        parser.add_argument('-columns', help='Comma separated list of columns to analyze', default='None')
        parser.add_argument('-date', help='date to look for a given parser', default='None')
        parser.add_argument('-s3Path', help='Source data location(Ceph bucket subfolder)', default='None')
        parser.add_argument('-s3endpointUrl', help='Ceph backend endpoint URL', default='None')
        parser.add_argument('-s3objectStoreLocation', help='Ceph Store (Bucket) Name', default='DH-DEV-INSIGHTS')
        parser.add_argument('-s3accessKey', help='Ceph Store Access Key', default='None')
        parser.add_argument('-s3secretKey', help='Ceph Store Secret key', default='None')
        parser.add_argument('-destination', help='Location to store results', default='correlation_analysis/result.json')
        args=parser.parse_args()

        cols = (args.columns).split(",")
        test = [{"name": args.parser, "columns": cols}]
        params_test = {"date": args.date, "parsers": test, "outfile": args.destination}
        PARAMS = json.dumps(params_test)
        params, parserInput = uf.decodeInputJson(PARAMS)
        date = args.date
   

        #Create s3 object
        clientKwargs = {'endpoint_url': args.s3endpointUrl}
        s3 = s3fs.S3FileSystem(secret=args.s3secretKey, key=args.s3accessKey, client_kwargs=clientKwargs)
        
        body = uf.getOutputJson('in_progress', [], [], args.s3objectStoreLocation, params)

        colNames, colTypes = uf.getColumnType(s3, os.path.join(args.s3objectStoreLocation, args.s3Path), parserInput)
        df = uf.readData(s3, args.s3objectStoreLocation, date, parserInput) #Read data
        df = uf.compressCat(df, colNames, colTypes)
        print(len(df))

        output = callDirector(df, colNames, colTypes) #Call analysis
        body = uf.getOutputJson('complete', output, [], args.s3objectStoreLocation, params)

        #Store result to Ceph
        uf.writetoCeph(s3, os.path.join(args.s3objectStoreLocation, args.destination), body, "Output Written to Ceph")
        print("The following is the json serialized list of graphs \n")
        print(body)
        print("Completed")
    
    except OSError:
        err = {'error':str(sys.exc_info()[0]), 'message':str(sys.exc_info()[1]), 'traceback':traceback.format_exc()}
        for i, j in err.items(): print(i, j, "\n")
        
    except:
        err = [{'error':str(sys.exc_info()[0]), 'message':str(sys.exc_info()[1]), 'traceback':traceback.format_exc()}]
        for i, j in err[0].items(): print(i, j, "\n")
        body = uf.getOutputJson('errors', [], err, args.s3objectStoreLocation, params)
        uf.writetoCeph(s3, os.path.join(args.s3objectStoreLocation, args.destination), body, "Error Written to Ceph")


